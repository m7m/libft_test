/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 02:22:20 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:10:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstdel_test0(void *content, size_t content_size);

t_list	*ft_lstmap_test0(t_list *ls)
{
	((char *)ls->content)[0] = 'B';
	return (ls);
}

static void	ft_lsputcontent(t_list *ls)
{
	ft_putstr((char *)ls->content);
	if (ls->next)
		ft_lsputcontent(ls->next);
}

void	ft_lstmap_test(void)
{
	t_list *ls;
	t_list *lsmap;

	ls = (t_list *)malloc(sizeof(t_list));
	ft_putstr(MSG_TEST1);
	ft_putstr("av: 012345678 ap: 012345678 ap2: B12B45B78 ~ ");
	ls = ft_lstnew("012", sizeof(char) * 4);
	ft_lstaddnext(ls, "345", sizeof(char) * 4);
	ft_lstaddnext(ls, "678", sizeof(char) * 4);
	ft_putstr("av: ");
	ft_lsputcontent(ls);
	lsmap = ft_lstmap(ls, &ft_lstmap_test0);
	ft_putstr(" ap: ");
	ft_lsputcontent(ls);
	ft_putstr(" ap2: ");
	ft_lsputcontent(lsmap);
	
	ft_lstdel(&ls, &ft_lstdel_test0);
	ft_lstdel(&lsmap, &ft_lstdel_test0);
	free(ls);
	free(lsmap);
}
