/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 01:28:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:11:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstdel_test0(void *content, size_t content_size);

static void	ft_lsputcontent(t_list *ls)
{
	ft_putstr((char *)ls->content);
	if (ls->next)
		ft_lsputcontent(ls->next);
}

void	ft_lstadd_test(void)
{
	t_list **asls;

	asls = (t_list **)malloc(sizeof(t_list *) * 4);
	ft_putstr(MSG_TEST1);
	ft_putstr("av: 012345678 ap: 999012345678 ~ ");
	*asls = ft_lstnew("012", sizeof(char) * 4);
	ft_lstaddnext(*asls, "345", sizeof(char) * 4);
	ft_lstaddnext(*asls, "678", sizeof(char) * 4);
	ft_putstr("av: ");
	ft_lsputcontent(*asls);
	ft_lstadd(asls, ft_lstnew("999", sizeof(char) * 4));
	ft_putstr(" ap: ");
	ft_lsputcontent(*asls);

	ft_lstdel(asls, &ft_lstdel_test0);
	free(asls);
}
