/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 21:27:00 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:11:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstdelone_test0(void *content, size_t content_size)
{
	free(content);
	content_size = 0;
}

void	ft_lstdelone_test(void)
{
	t_list *ls;
	char str[] = "asdfghjkl";

	ft_putstr(MSG_TEST1);
	ft_putstr("av: non NULL ap: NULL ~ ");
	ls = ft_lstnew(str, sizeof(char) * (ft_strlen(str) + 1));;
	
	if (!ls)
		ft_putstr("av: NULL");
	else
		ft_putstr("av: non NULL");
	ft_lstdelone(&ls, &ft_lstdelone_test0);

	if (!ls)
		ft_putstr(" ap: NULL ");
	else
		ft_putstr(" ap: non NULL ");
}
