/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 18:52:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:10:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstnew_test(void)
{
	t_list *ls;
	char str[] = "asdfghjkl";

	ft_putstr(MSG_TEST1);
	ft_putstr("asdfghjkl a0dfghjkl ~ ");
	ls = ft_lstnew(str, sizeof(char) * (ft_strlen(str) + 1));;
	((char *)ls->content)[1] = '0';
	ft_putstr(str);
	ft_putchar(' ');
	ft_putstr(ls->content);
	free(ls->content);
	free(ls);

	ft_putstr(MSG_TEST1);
	ft_putstr("content = NULL 0 ~ ");
	ls = ft_lstnew(NULL, 30);;
	if (!ls->content)
	{
		ft_putstr("content = NULL");
		ft_putchar(' ');
		ft_putchar(((char)ls->content_size + 48));
	}
	else
		ft_putstr("non null");
	free(ls);
}
