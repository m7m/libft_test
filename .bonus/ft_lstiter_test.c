/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 01:53:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:10:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstdel_test0(void *content, size_t content_size);

void	ft_lstiter_test0(t_list *ls)
{
	((char *)ls->content)[0] = 'A';
}

static void	ft_lsputcontent(t_list *ls)
{
	ft_putstr((char *)ls->content);
	if (ls->next)
		ft_lsputcontent(ls->next);
}

void	ft_lstiter_test(void)
{
	t_list *ls;

	ls = (t_list *)malloc(sizeof(t_list));
	ft_putstr(MSG_TEST1);
	ft_putstr("av: 012345678 ap: A12A45A78 ~ ");
	ls = ft_lstnew("012", sizeof(char) * 4);
	ft_lstaddnext(ls, "345", sizeof(char) * 4);
	ft_lstaddnext(ls, "678", sizeof(char) * 4);
	ft_putstr("av: ");
	ft_lsputcontent(ls);
	ft_lstiter(ls, &ft_lstiter_test0);
	ft_putstr(" ap: ");
	ft_lsputcontent(ls);

	ft_lstdel(&ls, &ft_lstdel_test0);
	free(ls);
}
