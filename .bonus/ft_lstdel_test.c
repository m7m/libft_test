/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 22:40:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:10:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include "libft_test_lst.h"

void	ft_lstdel_test0(void *content, size_t content_size)
{
	free(content);
	content_size = 0;
}

static void	ft_lsputcontent(t_list *ls)
{
	ft_putstr((char *)ls->content);
	if (ls->next)
		ft_lsputcontent(ls->next);
}

void	ft_lstdel_test(void)
{
	t_list **asls;

	asls = (t_list **)malloc(sizeof(t_list *) * 4);
	ft_putstr(MSG_TEST1);
	ft_putstr("012345678 012345678 345678 678 NULL ~ ");
	*asls = ft_lstnew("012", sizeof(char) * 4);
	ft_lstaddnext(*asls, "345", sizeof(char) * 4);
	ft_lstaddnext(*asls, "678", sizeof(char) * 4);
	ft_lsputcontent(*asls);
	asls[1] = (*asls)->next;
	asls[2] = (*asls)->next->next;

	ft_putchar(' ');
	ft_lsputcontent(asls[0]);
	ft_putchar(' ');
	ft_lsputcontent(asls[1]);
	ft_putchar(' ');
	ft_lsputcontent(asls[2]);

	ft_lstdel(asls, &ft_lstdel_test0);
	if (!asls[0])
		ft_putstr(" NULL");
	else
		ft_putstr(" non NULL");
	/* Decommenter pour verifier la liberation */
	/* free(asls[1]); */
	/* free(asls[2]); */

	free(asls);
}
