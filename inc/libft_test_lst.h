
#ifndef LIBFT_TEST_LST_H
# define LIBFT_TEST_LST_H

typedef struct	s_list
{
	void				*content;
	size_t				content_size;
	struct s_list	*next;
}							t_list;

void	ft_lstnew_test(void);
void	ft_lstdelone_test(void);
void	ft_lstdel_test(void);
void	ft_lstadd_test(void);
void	ft_lstiter_test(void);
void	ft_lstmap_test(void);
void	ft_lstaddnext(t_list *ls, void const *content, size_t content_size);

#endif
