/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_test.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 01:19:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:08:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_TEST_H
# define LIBFT_TEST_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include "libft.h"

# define MSG_TEST1 "\ntest "__FILE__" : \t"

void	*ft_memallocset(size_t size, int c);
void	ft_putnchar(char c, int n);
void	ft_putstrf(char const *s, char fc);

void	ft_putchar_test(void);
void	ft_putnbr_test(void);
void	ft_putstr_test(void);
void	ft_strcpy_test(void);
void	ft_strncpy_test(void);
void	ft_strcat_test(void);
void	ft_strncat_test(void);
void	ft_strlcat_test(void);
void	ft_strchr_test(void);
void	ft_strrchr_test(void);
void	ft_strstr_test(void);
void	ft_strnstr_test(void);
void	ft_strcmp_test(void);
void	ft_strncmp_test(void);
void	ft_atoi_test(void);
void	ft_isalpha_test(void);
void	ft_isdigit_test(void);
void	ft_isalnum_test(void);
void	ft_isascii_test(void);
void	ft_isprint_test(void);

void	ft_toupper_test(void);
void	ft_tolower_test(void);
void	ft_strlen_test(void);
void	ft_itoa_test(void);
void	ft_strmap_test(void);
void	ft_strdup_test(void);
void	ft_memalloc_test(void);
void	ft_memdel_test(void);
void	ft_memset_test(void);
void	ft_memcpy_test(void);
void	ft_memccpy_test(void);
void	ft_memmove_test(void);
void	ft_memchr_test(void);
void	ft_memcmp_test(void);
void	ft_bzero_test(void);

void	ft_strsplit_test(void);
void	ft_putendl_test(void);
void	ft_putchar_fd_test(void);
void	ft_putstr_fd_test(void);
void	ft_putendl_fd_test(void);
void	ft_putnbr_fd_test(void);

void	ft_strclr_test(void);
void	ft_strdel_test(void);
void	ft_strnequ_test(void);
void	ft_strequ_test(void);
void	ft_striter_test(void);
void	ft_striteri_test(void);
void	ft_strjoin_test(void);
void	ft_strmapi_test(void);
void	ft_strsub_test(void);
void	ft_strtrim_test(void);

#endif
