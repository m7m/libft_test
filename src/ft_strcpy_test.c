/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:58:43 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/03 22:50:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strcpy_test(void)
{
	char *dst;

	dst = (char *)ft_memallocset(sizeof(char) * 10, 48);
	if (dst == NULL)
		ft_putstr("alloc NULL");
	ft_putstr(MSG_TEST1);
	ft_putstr("1234500000 ~ ");
	dst = ft_strncpy(dst, "123456789", 5);
	ft_putstr(dst);
	ft_putstr(MSG_TEST1);
	ft_putstr("1234500000 ~ ");
	dst = ft_strncpy(dst, "123456789", 5);
	ft_putstr(dst);
}
