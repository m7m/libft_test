/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 07:01:37 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 07:10:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putchar_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("h ~ ");
	ft_putchar('h');
	ft_putstr(MSG_TEST1);
	ft_putstr("ESC ~ ^[ ~ ");
	ft_putchar(27);
}
