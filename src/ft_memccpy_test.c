/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 09:47:27 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/12 13:29:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memccpy_test0(char *src, char *dst)
{
	char *ret;

	ft_putstr(MSG_TEST1);
	ft_putstr("ret=NULL src=456 dst=451[.*] ~ ret=");
	if ((ret = ft_memccpy(dst, src, 47, 2)) == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(MSG_TEST1);
	ft_putstr("ret=1[.*] src=456 dst=451[.*] ~ ret=");
	ret = ft_memccpy(dst, src, 53, 2);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" dst=");
	ft_putstr(dst);
}

void	ft_memccpy_test(void)
{
	char dst[4];

	dst[0] = '3';
	dst[1] = '2';
	dst[2] = '1';
	ft_memccpy_test0("456", dst);
}
