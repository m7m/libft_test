/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 20:12:27 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 16:36:12 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putendl_fd_test(void)
{
	int fd;

	//fd = open("test_putendl_fd", O_CREAT|O_WRONLY, 0644);
	fd = STDOUT_FILENO;
	
	ft_putstr(MSG_TEST1);
	ft_putstr("vn;oash dfbet wewf\\n\\n ~ ");
	ft_putendl_fd("vn;oash dfbet wewf\n", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("-1\\n ~ ");
	ft_putendl_fd("-1", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("0\\n ~ ");
	ft_putendl_fd("0", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("-1\\n ~ ");
	ft_putendl_fd("-1", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("\\n ~ ");
	ft_putendl_fd("", fd);
}
