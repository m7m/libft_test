
#include <stdio.h>
#include "libft_test.h"

/* void ft_btoa_base16_test(void) */
/* { */
/* //	long i = 0b0100011100011101011100001010001111010111000010100100; */
/* 	char *str; */

/* 	/\* str = ft_btoa_base16(&i, sizeof(long)); *\/ */
/* 	/\* printf("s:%s lx:%lX ld:%ld\n", str, i, i);fflush(stdout); *\/ */
/* 	/\* /\\* while (j < 16) *\\/ *\/ */
/* 	/\* /\\* 	printf("%c", str[j++]); *\\/ *\/ */
/* 	/\* free(str); *\/ */

/* 	double d = 0b0100011100011101011100001010001111010111000010100100; */
/* 	str = ft_dtoa_base16(d); */
/* 	printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout); */
/* 	/\* while (j < 16) *\/ */
/* 	/\* 	printf("%c", str[j++]); *\/ */
/* 	free(str); */
	
/* 	str = ft_dtoa_base16(d = 0); */
/* 	printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = -0.0001); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = 1); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = 5); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = 0x1.1F5P+0); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = 2); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */
/* 	str = ft_dtoa_base16(d = 3); */
/* 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */

/* 	str = ft_dtoa_base16(d = 0.0001); */
/* 	printf("s:%s f:%f \na:%.2A %e\n", str, d, d, d);fflush(stdout); */
/* 	free(str); */

/* 	/\* printf("%f %f \n%f \n%f %d %f \n", 1.25*((double)1/2), 1.25*(0.5), *\/ */
/* 	/\* 	   (1.+1.*(1./(1 << 6))+1.*(1./(1 << 7))+1.*(1./(1 << 10))+1.*(1./(1 << 11)))*(2 << 8), *\/ */
/* 	/\* 	   (1.*(2 << 8) + 1.*(1./(1 << 6))*(2 << 8) + 1.*(1./(1 << 7))*(2 << 8) + 1.*(1./(1 << 10))*(2 << 8) + 1.*(1./(1 << 11))*(2 << 8)), *\/ */
/* 	/\* 	   (1 << 10), *\/ */
/* 	/\* 	   1.*(1.D/(1 << 11)) *\/ */
/* 	/\* 	); *\/ */
/* 	/\* printf(" %f %f %f %f %f\n", *\/ */
/* 	/\* 	   1.*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 6))*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 7))*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 10))*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 11))*(2 << 8) *\/ */
/* 	/\* 	); *\/ */
/* 	/\* printf(" %f %f %f %f %f\n", *\/ */
/* 	/\* 	   1.*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 6))*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 7))*(2 << 8), *\/ */
/* 	/\* 	   1.*(1./(1 << 10))*(2 << 8) * 10, *\/ */
/* 	/\* 	   1.*(1./(1 << 11))*(2 << 8) * 10 * 10 *\/ */
/* 	/\* 	); *\/ */

/* 	printf("%f %d", 2.29*10, (int)(2.29*10)); */
/* } */
