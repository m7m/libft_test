/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 01:03:47 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 15:23:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_TestMemCpy(void);
void	ft_TestMemMove(void);

#include "libft_test.h"

void	ft_separat(char *str)
{
	int len = ft_strlen(str);

	ft_putchar('\n');
	ft_putnchar('#', (40 - (len / 2) - 1));
	ft_putchar(' ');
	ft_putstr(str);
	ft_putchar(' ');
	ft_putnchar('#', (40 - (len / 2 + len % 2) - 1));
}

void	ft_part1(void)
{
	ft_separat("memset/bzero");
	ft_memset_test();
	ft_bzero_test();
	ft_separat("memcpy/memccpy");
	ft_memcpy_test();
	ft_TestMemCpy();
	ft_memccpy_test();
	ft_separat("memmove");
	ft_memmove_test();
	ft_TestMemMove();
	ft_separat("memchr/memcmp");
	ft_memchr_test();
	ft_memcmp_test();
	ft_separat("str");
	ft_strlen_test();
	ft_strdup_test();
	ft_strcpy_test();
	ft_strncpy_test();
	ft_strcat_test();
	ft_strncat_test();
	ft_strlcat_test();
	ft_strchr_test();
	ft_strrchr_test();
	ft_strstr_test();
	ft_strnstr_test();
	ft_strcmp_test();
	ft_strncmp_test();
	ft_separat("atoi");
	ft_atoi_test();
	ft_separat("is");
	ft_isalpha_test();
	ft_isdigit_test();
	ft_isalnum_test();
	ft_isascii_test();
	ft_isprint_test();
	ft_separat("upper/lower");
	ft_toupper_test();
	ft_tolower_test();
}

void	ft_part2(void)
{
	ft_separat("memalloc/memdel");
	ft_memalloc_test();
	ft_memdel_test();
	/* ft_strnew_test(); */ /* n'existe pas */
	ft_separat("strdel/strclr");
	ft_strdel_test();
	ft_strclr_test();
	ft_separat("striter/striteri");
	ft_striter_test();
	ft_striteri_test();
	ft_separat("strmap/strmapi");
	ft_strmap_test();
	ft_strmapi_test();
	ft_separat("strequ/strnequ");
	ft_strequ_test();
	ft_strnequ_test();
	ft_separat("strsub");
	ft_strsub_test();
	ft_separat("strjoin");
	ft_strjoin_test();
	ft_separat("strtrim");
	ft_strtrim_test();
	ft_separat("strsplit");
	ft_strsplit_test();
	ft_separat("itoa");
	ft_itoa_test();
	ft_separat("put");
	ft_putchar_test();
	ft_putstr_test();
	ft_putendl_test();
	ft_putnbr_test();
	ft_separat("put fd");
	ft_putchar_fd_test();
	ft_putstr_fd_test();
	ft_putendl_fd_test();
	ft_putnbr_fd_test();
}

/* ls .bonus */
/* void	ft_part_3_bonus(void) */
/* { */
/* 	ft_separat("lst"); */
/* 	ft_lstnew_test(); */
/* 	ft_lstdelone_test(); */
/* 	ft_lstdel_test(); */
/* 	ft_lstadd_test(); */
/* 	ft_lstiter_test(); */
/* 	ft_lstmap_test(); */
/* } */

int	main(void)
{
	ft_separat("");
	ft_separat("Part 1");
	ft_separat("");
	ft_part1();
	ft_separat("");
	ft_separat("Part 2");
	ft_separat("");
	ft_part2();
	ft_separat("");
	/* ft_separat("Part 3 bonus"); */
	/* ft_separat(""); */
	/* ft_part_3_bonus(); */
	ft_putstr("\n -- Test fini -- \n");	
	return (0);
}
