/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:50:11 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/22 23:31:12 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strcat_test(void)
{
	char *dst;
	char *src;

	dst = (char *)ft_memalloc(sizeof(char) * 3);
	dst[0] = 'A';
	dst[1] = 'b';
	dst[2] = 'c';
	src = (char *)malloc(sizeof(char) * 15);
	src[0] = '1';
	src[1] = '2';
	src[2] = '3';
	src[3] = '4';
	ft_putstr(MSG_TEST1);
	ft_putstr("av: dst=Abc, ap: Abc[?]1234 dst=Abc[?]1234 src=1234 ~ ");
	ft_putstr(ft_strcat(dst, src));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	free(dst);
	free(src);
}
