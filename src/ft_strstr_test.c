/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 02:56:20 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/22 20:09:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strstr_test(void)
{
	char s[6];
	char *ret;

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	s[5] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("bce ~ ");
	ret = ft_strstr(s, "bc");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ret = ft_strstr("", "");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("bbce ~ ");
	ret = ft_strstr(s, "b");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 0;
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr(s, "bc");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("bcde ~ ");
	ret = ft_strstr(s, "bc");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("abcde ~ ");
	ret = ft_strstr(s, "");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr(s, "be");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr("", "tyuio");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr(". ~ ");
	ret = ft_strstr("asd.", ".");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr(". ~ ");
	ret = ft_strstr("Ceci n'est pas une pipe.", ".");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("ozaraboze ~ ");
	ret = ft_strstr("ozarabozaraboze", "ozaraboze");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr("ozaraboze", "ozarabozaraboze");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr(NULL, "");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strstr("", "abcde");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("abcdeabcde ~ ");
	ret = ft_strstr("abcdeabcde", "abcde");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("abcde ~ ");
	ret = ft_strstr("abrdeabcde", "abcde");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("ab\\200deabcde || abM-^@deabcde ~ ");
	ret = strstr("ab\200deabcde", "ab\200de");
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
}
