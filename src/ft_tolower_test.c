/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 08:21:31 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/28 08:21:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_tolower_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("p ~ ");
	ft_putchar(ft_tolower('P'));
	ft_putstr(MSG_TEST1);
	ft_putstr("u ~ ");
	ft_putchar(ft_tolower('U'));
	ft_putstr(MSG_TEST1);
	ft_putstr("5 ~ ");
	ft_putchar(ft_tolower('5'));
	ft_putstr(MSG_TEST1);
	ft_putstr("# ~ ");
	ft_putchar(ft_tolower('#'));
	ft_putstr(MSG_TEST1);
	ft_putstr("127 ~ ");
	ft_putnbr(ft_tolower(0177));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_tolower(0));
	ft_putstr(MSG_TEST1);
	ft_putstr("255 ~ ");
	ft_putnbr(ft_tolower(0xff));
}
