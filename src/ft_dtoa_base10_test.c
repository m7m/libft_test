/* char			*ft_ltoa(long n); */

/* /\* */
/* ** m.mmme+-e */
/* ** tab : 0 = cast n, 1 = exposant, 2 = negation */
/* **  5 = exposant temporaire */
/* ** */
/* *\/ */
/* /\* char		*ft_dtoa_base10_anc(double n) *\/ */
/* /\* { *\/ */
/* /\* 	char	*str; *\/ */
/* /\* 	t_ul	ltab[5]; *\/ */
/* /\* 	long	i; *\/ */
/* /\* 	int		neg; *\/ */
/* /\* 	int		expo; *\/ */
	
/* /\* 	neg = 0; *\/ */
/* /\* 	ln = *(long *)&n; *\/ */
/* /\* 	if (ln & ((unsigned long)1 << (sizeof(long) * 8 - 1))) *\/ */
/* /\* 		neg = 1; *\/ */
/* /\* 	expo = ((ln >> (sizeof(long) * 8 - 12)) & 0b11111111111); *\/ */

/* /\* 	i = (long)n; *\/ */
/* /\* 	ltab[3] = (expo -= 1023); *\/ */
/* /\* 	printf("\nn=f:%f ", n); *\/ */
/* /\* 	while ((double)i != n) *\/ */
/* /\* 	{ *\/ */
/* /\* 		if (n * 10 > (double)((long)1 << (63 / 2))) *\/ */
/* /\* 			break ; *\/ */
/* /\* 		n *= 10; *\/ */
/* /\* 		i = (long)n; *\/ */
/* /\* 		ltab[3] -= 1; *\/ */
/* /\* 	} *\/ */

/* /\* 	if (!(str = ft_strnew(sizeof(char) * 8))) *\/ */
/* /\* 		return (NULL); *\/ */

/* /\* 	printf("i:%ld expo:%ld expo_mod:%ld\n", i, ltab[1], ltab[3]); *\/ */
/* /\* 	ltab[0] = (t_ul)i; *\/ */
/* /\* 	printf("i:%ld expo:%ld f:%f \n", ltab[0], ltab[3] - ltab[1], n); *\/ */
/* /\* 	printf("s:%s\n", ft_ltoa(ltab[0])); *\/ */

/* /\* 	return (str); *\/ */
/* /\* } *\/ */

/* char		*ft_dtoa_base10_nopreci(double n, int precision) */
/* { */
/* 	char	*str; */
/* //	t_ul	ltab[5]; */
/* 	int		nbc; */
/* 	int		neg; */
/* 	int		expo; */
/* 	t_uc	*cn; */
/* 	double	valprecision; */

/* 	valprecision = 1; */
/* 	expo = ++precision; */
/* 	while (--expo) */
/* 		valprecision /= 10; */
/* 	printf(" n in f:%.8f %.8f \t", n, valprecision); */

/* 	cn = (t_uc *)&n; */
/* 	if ((neg = !!(cn[sizeof(double) - 1] & (1 << (sizeof(char) * 8 - 1))))) */
/* 		n = -n; */
/* 	expo = 0; */
/* 	while (n > 1) */
/* 	{ */
/* 		expo += 1; */
/* 		n /= 10.; */
/* 	} */
/* 	if (!(str = ft_strnew(sizeof(char) * (expo + precision + 2)))) */
/* 		return (NULL); */
/* 	nbc = -1; */
/* 	if (neg) */
/* 		str[++nbc] = '-'; */
/* 	if (!expo) */
/* 	{ */
/* 		str[++nbc] = 48; */
/* 		expo += 1; */
/* 	} */
/* 	while (++nbc < precision + expo + neg) */
/* 	{ */
/* 		n = (n - (int)n) * 10; */
/* 		if (nbc == expo + neg) */
/* 			str[nbc++] = '.'; */
/* //		if ((int)(n + (double)0.1) < 10) */
/* //			str[nbc] = (int)n + 48; */
/* 		/\* else *\/ */
/* 		/\* 	str[nbc] = (int)n; *\/ */

/* 		/\* if ((int)(n + 0.1) == (int)n) *\/ */
/* 		/\* 	str[nbc] = (int)(n + 0.1) + 48; *\/ */
/* 		/\* else *\/ */
/* 		/\* 	str[nbc] = (int)n + 48; *\/ */
/* //		str[nbc] = (int)(n + valprecision) + 48; */
/* 		if ((int)(n + valprecision) < 10) */
/* 			str[nbc] = (int)n + 48; */
/* 		else */
/* 			str[nbc] = 48; */
/* 	}		 */

/* 	printf("n:%f expo:%d neg:%d nbc:%d s:%s \n", n, expo, neg, nbc, str); */

/* 	return (str); */
/* } */

/* double	ft_modulo_troplong(double a, double diviseur) */
/* { */
/* 	while (a >= diviseur) */
/* 	{ */
/* //		printf(" %f %f", a, diviseur); */
/* //		return (a); */
/* 		a -= diviseur; */
/* 	} */
/* 	return (a); */
/* } */

/* /\* char		*ft_dtoa_base10_test(double n, int precision) *\/ */
/* /\* { *\/ */
/* /\* 	char	*str; *\/ */
/* /\* 	int		nbc; *\/ */
/* /\* 	int		neg; *\/ */
/* /\* 	int		expo; *\/ */
/* /\* 	t_uc	*cn; *\/ */
/* /\* 	double	valprecision; *\/ */
/* /\* 	double	multip; *\/ */
/* /\* 	double	reste; *\/ */

/* /\* 	valprecision = 1; *\/ */
/* /\* 	expo = ++precision; *\/ */
/* /\* 	while (--expo) *\/ */
/* /\* 		valprecision /= 10; *\/ */
/* /\* 	printf(" n in f:%.20f %.8f \t", n, valprecision); *\/ */

/* /\* 	cn = (t_uc *)&n; *\/ */
/* /\* 	if ((neg = !!(cn[sizeof(double) - 1] & (1 << (sizeof(char) * 8 - 1))))) *\/ */
/* /\* 		n = -n; *\/ */
/* /\* 	expo = 0; *\/ */
/* /\* 	while (n > 1) *\/ */
/* /\* 	{ *\/ */
/* /\* 		expo += 1; *\/ */
/* /\* 		n /= 10.; *\/ */
/* /\* 	} *\/ */
/* /\* 	if (!(str = ft_strnew(sizeof(char) * (expo + precision + 2)))) *\/ */
/* /\* 		return (NULL); *\/ */
/* /\* 	nbc = -1; *\/ */
/* /\* 	if (neg) *\/ */
/* /\* 		str[++nbc] = '-'; *\/ */
/* /\* 	if (!expo) *\/ */
/* /\* 	{ *\/ */
/* /\* 		str[++nbc] = 48; *\/ */
/* /\* 		expo += 1; *\/ */
/* /\* 	} *\/ */
/* /\* 	multip = 1; *\/ */
/* /\* //	printf("\n"); *\/ */
/* /\* 	while (++nbc < precision + expo + neg) *\/ */
/* /\* 	{ *\/ */
/* /\* 		multip *= 10; *\/ */
		
/* /\* 		if (nbc == expo + neg) *\/ */
/* /\* 			str[nbc++] = '.'; *\/ */
/* /\* 		str[nbc] = ((n * multip) - (unsigned long)(n * multip) / 10 * 10) + 48; *\/ */
/* /\* //		str[nbc] = ((unsigned long)(n * multip) % 10) + 48; *\/ */

/* /\* 		if ((reste = ft_modulo(n * multip, 10)) <= 9) *\/ */
/* /\* 			str[nbc] = (int)reste + 48; *\/ */
/* /\* 		else *\/ */
/* /\* 			str[nbc] = 48; *\/ */
/* /\* 		printf(" %c %f \n", str[nbc], reste); *\/ */
/* /\* 	}		 *\/ */

/* /\* 	printf("n:%f expo:%d neg:%d nbc:%d s:%s\n", n, expo, neg, nbc, str); *\/ */

/* /\* 	return (str); *\/ */
/* /\* } *\/ */

/* char		*ft_dtoa_base10_tt(double n, int precision) */
/* { */
/* 	char	*str; */
/* 	int		nbc; */
/* 	int		neg; */
/* 	int		expo; */
/* 	t_uc	*cn; */
/* 	double	valprecision; */
/* 	double	multip; */
/* 	double	reste; */

/* 	valprecision = 9; */
/* 	expo = ++precision; */
/* 	while (--expo) */
/* 		valprecision = valprecision / 10 + 9; */
/* 	printf(" n in f:%.16f %.8f \t", n, valprecision); */

/* 	cn = (t_uc *)&n; */
/* 	if ((neg = !!(cn[sizeof(double) - 1] & (1 << (sizeof(char) * 8 - 1))))) */
/* 		n = -n; */
/* 	expo = 0; */
/* 	while (n > 1) */
/* 	{ */
/* 		expo += 1; */
/* 		n /= 10.; */
/* 	} */
/* 	if (!(str = ft_strnew(sizeof(char) * (expo + precision + 2)))) */
/* 		return (NULL); */
/* 	nbc = -1; */
/* 	if (neg) */
/* 		str[++nbc] = '-'; */
/* 	if (!expo) */
/* 	{ */
/* 		str[++nbc] = 48; */
/* 		expo += 1; */
/* 	} */
/* 	multip = 1; */
/* //	printf("\n"); */
/* 	while (++nbc < precision + expo + neg) */
/* 	{ */
/* 		multip *= 10; */
		
/* 		if (nbc == expo + neg) */
/* 			str[nbc++] = '.'; */
/* 		reste = ((n * multip) - (unsigned long)(n * multip) / 10 * 10); */
/* //		reste = ft_modulo(n * multip, 10); */
/* 		if (reste <= valprecision) */
/* 			str[nbc] = (int)reste + 48; */
/* 		else */
/* 			str[nbc] = 48; */
/* //		printf(" %c %f \n", str[nbc], reste); */
/* 	}		 */

/* 	printf("n:%f expo:%d neg:%d nbc:%d s:%s\n", n, expo, neg, nbc, str); */

/* 	return (str); */
/* } */

/* /////////////////////////////////////////////////////////////////////// */


/* double	ft_modulo(double a, double diviseur) */
/* { */
/* 	double	i; */

/* 	i = diviseur; */

/* 	while (a >= i) */
/* 		i *= diviseur; */
	
/* //	printf(" mod 1p i:%f a:%f ", i, a);fflush(stdout); */
/* 	while (a >= i) */
/* 		i += diviseur; */
/* //	printf(" mod 2p ");fflush(stdout); */
/* 	while (a < i) */
/* 		i -= diviseur; */
/* //	printf(" mod fin ");fflush(stdout); */
/* 	a -= i; */
/* 	return (a); */
/* } */

/* double	ft_mod10(double a) */
/* { */
/* 	double	i; */
/* 	i = 10; */
/* 	i = i * a / i - i; */
	
/* 	a -= i; */
/* 	return (a); */
/* } */

/* double	ft_modulo_tt(double a, double diviseur) */
/* { */
/* 	double	i; */

/* //	i = (a - 1) / diviseur; */
/* 	i = (a - 1) / diviseur; */

/* 	while (i < a) */
/* 		i += diviseur; */

/* 	a -= i - diviseur; */
/* 	return (i); */
/* } */


/* char		*ft_dtoa_base10_pasencore(double n, int precision) */
/* { */
/* 	char	*str; */
/* 	int		nbc; */
/* 	int		neg; */
/* 	int		expo; */
/* 	double	tmpn; */

/* 	precision++; */
/* 	printf(" n in f:%30.6f \t", n); */

/* 	if ((neg = (n < 0))) */
/* 		n = -n; */
/* 	expo = 0; */
/* 	tmpn = n; */
/* 	while (tmpn >= 1) */
/* 	{ */
/* 		expo += 1; */
/* 		tmpn /= 10.; */
/* 	} */
/* 	if (!(str = ft_strnew(sizeof(char) * (expo + precision + 2)))) */
/* 		return (NULL); */
/* 	nbc = -1; */
/* 	if (neg) */
/* 		str[++nbc] = '-'; */
/* 	if (!expo) */
/* 	{ */
/* 		str[++nbc] = 48; */
/* 		expo += 1; */
/* 	} */
/* 	tmpn = n; */
/* 	nbc = expo + neg; */
/* 	precision += expo + neg; */
/* 	while (n >= 1.) */
/* 	{ */
		
/* 		str[--expo + neg] = (unsigned long)(n - (unsigned long)n / 10 * 10) + 48; // precission total dans la limite du long */
/* //		str[--expo + neg] = (int)ft_modulo(n, 10) + 48; */
/* 		n /= 10.; */

/* 		printf("%u ", (int)ft_modulo(n, 10)); */
/* 	} */
/* 	str[nbc] = '.'; */

/* 	n = tmpn; */
/* 	while (++nbc < precision) */
/* 	{ */
/* 		if (nbc == expo + neg) */
/* 			str[nbc++] = '.'; */

/* 		n *= 10; */
/* 		str[nbc] = (unsigned long)(n - (unsigned long)n / 10 * 10) + 48; // precission total dans la limite du long */

/* //		printf("%u ", (unsigned int)n); */
/* 	}		 */

/* 	printf("expo:%d neg:%d nbc:%d preci:%d s:%30s\n", expo, neg, nbc, precision, str); */

/* 	return (str); */
/* } */
  
