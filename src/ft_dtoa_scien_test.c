
#include <stdio.h>
#include "libft_test.h"

int main(void)
{
	char	*str;
	double	a;

	str = ft_dtoa_scien(a = 5255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 52.55, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = .5255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = .00005255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = -5255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = -52.55, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = -.5255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = -.00005255, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 0, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 1.9999, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 1., 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 2, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa_scien(a = 207976931348623169, 6);
	printf("e:%30e\ns:%30s\n", a, str);fflush(stdout);free(str);
}
