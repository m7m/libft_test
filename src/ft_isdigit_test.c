/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 07:41:44 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/28 07:47:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_isdigit_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_isdigit('o'));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_isdigit('U'));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isdigit('5'));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_isdigit('#'));
}
