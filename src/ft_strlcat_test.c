/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 20:50:17 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/27 23:33:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strlcat_test(void)
{
	char *src;
	char *dst;

	// dst plus grand que size
	dst = (char *)ft_memallocset(sizeof(char) * 20, 57);
	dst[0] = 'a';
	dst[1] = 'b';
	dst[2] = 'c';
	dst[19] = 0;
	src = (char *)malloc(sizeof(char) * 5);
	src[0] = '1';
	src[1] = '2';
	src[2] = '3';
	src[3] = '4';
	src[4] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("9 dst=abc9999999999999999 src=1234 ~ ");
	ft_putnbr(ft_strlcat(dst, src, 5));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	free(src);
	free(dst);

	// dst plus petit que size et size est plus grand que dst + src
	dst = (char *)ft_memallocset(sizeof(char) * 20, 57);
	dst[0] = 'd';
	dst[1] = 'e';
	dst[2] = 'f';
	dst[3] = 0;
	src = (char *)malloc(sizeof(char) * 4);
	src[0] = '5';
	src[1] = '6';
	src[2] = '7';
	src[3] = '8';
	src[3] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("6 dst=def567 src=567 - 67^@9999 ~ ");
	ft_putnbr(ft_strlcat(dst, src, 20));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" - ");
	ft_putchar(dst[4]);
	ft_putchar(dst[5]);
	ft_putchar(dst[6]);
	ft_putchar(dst[7]);
	ft_putchar(dst[8]);
	ft_putchar(dst[9]);
	ft_putchar(dst[10]);
	free(src);
	free(dst);

	dst = (char *)ft_memallocset(sizeof(char) * 20, 57);
	dst[0] = 'd';
	dst[1] = 'e';
	dst[2] = 'f';
	dst[3] = 0;
	src = (char *)malloc(sizeof(char) * 4);
	src[0] = '5';
	src[1] = '6';
	src[2] = '7';
	src[3] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("6 dst=def5 src=567 - ^@99999 ~ ");
	ft_putnbr(ft_strlcat(dst, src, 5));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" - ");
	ft_putchar(dst[4]);
	ft_putchar(dst[5]);
	ft_putchar(dst[6]);
	ft_putchar(dst[7]);
	ft_putchar(dst[8]);
	ft_putchar(dst[9]);
	ft_putchar(dst[10]);
	free(src);
	free(dst);

	dst = (char *)ft_memallocset(sizeof(char) * 20, 57);
	dst[0] = 'd';
	dst[1] = 'e';
	dst[2] = 'f';
	dst[3] = 'g';
	dst[4] = 'h';	
	dst[5] = 'i';
	dst[6] = 0;
	src = (char *)malloc(sizeof(char) * 4);
	src[0] = '5';
	src[1] = '6';
	src[2] = 0;
	src[3] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("8 dst=defghi src=56 - hi9999 ~ ");
	ft_putnbr(ft_strlcat(dst, src, 6));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" - ");
	ft_putchar(dst[4]);
	ft_putchar(dst[5]);
	ft_putchar(dst[7]);
	ft_putchar(dst[8]);
	ft_putchar(dst[9]);
	ft_putchar(dst[10]);
	free(src);
	free(dst);
}
