/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 07:52:29 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/28 08:10:22 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_isascii_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii('o'));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii('U'));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii('5'));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii('#'));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii(0177));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isascii(0));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_isascii(0xff));
}
