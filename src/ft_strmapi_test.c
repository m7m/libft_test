/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 23:27:32 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:48:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

char	ft_strmapi_test0(unsigned int i, char c)
{
	if (c)
		c = i + 48;
	return (c);
}

void	ft_strmapi_test(void)
{
	char str[] = "basdfgz";
	char *strmapi;

	ft_putstr(MSG_TEST1);
	ft_putstr("basdfgz 0123456^@ ~ ");
	strmapi = ft_strmapi(str, &ft_strmapi_test0);
	ft_putstr(str);
	ft_putchar(' ');
	ft_putstr(strmapi);
	ft_putchar(strmapi[7]);
	free(strmapi);
}
