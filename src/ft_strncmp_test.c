/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 05:37:57 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:40:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strncmp_test(void)
{
	char c200[2];
	char c0[2];

	c200[0] = 200;
	c0[0] = 200;
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strncmp("abcde","abcde", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("-1 ~ ");
	ft_putnbr(ft_strncmp("abcde","abde", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("16 ~ ");
	ft_putnbr(ft_strncmp("qbcde","abde", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strncmp("abc","abcd", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strncmp("abcd","abc", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("97 ~ ");
	ft_putnbr(ft_strncmp("a","", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strncmp("","", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("-97 ~ ");
	ft_putnbr(ft_strncmp("","a", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("-200 ~ ");
	ft_putnbr(ft_strncmp("\0",c200, 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("200 ~ ");
	ft_putnbr(ft_strncmp(c0,"\0", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(strncmp("q","a", 0));
}
