
#include "libft_test.h"

void	ft_itoa_s_test0(char *val, int ival)
{
	char str[2048];
	int len;

	write(1, val, strlen(val));
	len = ft_itoa_s(ival, str);
	write(1, "\n", 1);
}

void	ft_itoa_s_test(void)
{
	ft_itoa_s_test0("0 ~ ", 0);
	ft_itoa_s_test0("-1 ~ ", -1);
	ft_itoa_s_test0("1 ~ ", 1);
	ft_itoa_s_test0("987654321 ~ ", 987654321);
	ft_itoa_s_test0("-123456 ~ ", -123456);
	ft_itoa_s_test0("-2147483648 ~ ", -2147483648);
}
