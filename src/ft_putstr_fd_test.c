/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 19:17:37 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 16:36:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putstr_fd_test(void)
{
	int fd;

	fd = STDOUT_FILENO;
	ft_putstr(MSG_TEST1);
	ft_putstr("asd ~ ");
	ft_putstr_fd("asd", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("2304902=3!@#)@#( ~ ");
	ft_putstr_fd("2304902=3!@#)@#(", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr_fd("\0", fd);
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr_fd("", fd);
}
