/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 06:38:56 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 23:41:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

char	ft_strmap_test0(char c)
{
	return (c + 32);
}

void	ft_strmap_test(void)
{
	char *str;

	ft_putstr(MSG_TEST1);
	ft_putstr("asdfgz ~ ");
	str = ft_strmap("ASDFGZ", &ft_strmap_test0);
	ft_putstr(str);
	free(str);
}
