/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 00:15:11 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:09:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strrchr_test(void)
{
	char s[6];

	s[0] = 'a';
	s[1] = 'c';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	s[5] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr("ce ~ ");
	ft_putstr(ft_strrchr(s, 'c'));

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	if (ft_strchr(s, 'g') == NULL)
		ft_putstr("NULL");
	else
		ft_putstr("non NULL");


	s[0] = 'a';
	s[1] = 'c';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr(ft_strrchr(s, 0));
}
