/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 03:58:22 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:39:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strcmp_test(void)
{
	char c200[2];
	char c0[2];

	c200[0] = 200;
	c0[0] = 200;
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strcmp("abcde","abcde"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-1 ~ ");
	ft_putnbr(ft_strcmp("abcde","abde"));
	ft_putstr(MSG_TEST1);
	ft_putstr("16 ~ ");
	ft_putnbr(ft_strcmp("qbcde","abde"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-100 ~ ");
	ft_putnbr(ft_strcmp("abc","abcd"));
	ft_putstr(MSG_TEST1);
	ft_putstr("100 ~ ");
	ft_putnbr(ft_strcmp("abcd","abc"));
	ft_putstr(MSG_TEST1);
	ft_putstr("97 ~ ");
	ft_putnbr(ft_strcmp("a",""));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strcmp("",""));
	ft_putstr(MSG_TEST1);
	ft_putstr("-97 ~ ");
	ft_putnbr(ft_strcmp("","a"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-200 ~ ");
	ft_putnbr(ft_strcmp("\0",c200));
	ft_putstr(MSG_TEST1);
	ft_putstr("200 ~ ");
	ft_putnbr(ft_strcmp(c0,"\0"));
}
