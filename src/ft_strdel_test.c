/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 19:17:46 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/03 17:31:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strdel_test(void)
{
	char **as;
	int i;

	i = 5;
	as = (char **)malloc(sizeof(as) * 6);
	while (i--)
		as[i] = (char *)ft_memallocset(sizeof(char) * 5, 50 + i);
	ft_putstr(MSG_TEST1);

	i = 5;
	ft_putstr("av: ");
	while (i--)
		ft_putstr(as[i]);
	ft_putstr(" ap:66666555554444433333NULL ~ ");
	i = 5;
	ft_strdel(as);
	while (i--)
		if (!as[i])
			ft_putstr("NULL ");
		else
			ft_putstr(as[i]);
	free(as);
}
