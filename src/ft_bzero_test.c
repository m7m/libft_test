/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero_test.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 08:40:33 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/30 16:50:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_bzero_test(void)
{
	char *str;

	str = (char *)ft_memalloc(sizeof(char) * 7);
	ft_memset(str, 75, 6);
	ft_putstr(MSG_TEST1);
	ft_putstr("^@^@^@KKK ~ ");
	ft_bzero(str, 3);
	ft_putchar(str[0]);
	ft_putchar(str[1]);
	ft_putchar(str[2]);
	ft_putchar(str[3]);
	ft_putchar(str[4]);
	ft_putchar(str[5]);
	ft_putstr(MSG_TEST1);
	ft_memset(str, 76, 6);
	ft_putstr("LLLLLL ~ ");
	ft_bzero(str, 0);
	ft_putstr(str);
	free(str);
}

