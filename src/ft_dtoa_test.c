
#include "libft_test.h"
#include <stdio.h>

void	ft_dtoa_test(void)
{
	char	*str;
	double	a;

	str = ft_dtoa(a = 5255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 52.55, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 5.255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .5255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .00005255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -5255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -52.55, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.5255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.00005255, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 0, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 1.9999, 20);
	printf("f:%30.20f\ns:%30s\n", a, str);fflush(stdout);free(str);

	str = ft_dtoa(a = 5255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 52.55, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .5255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .00005255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -5255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -52.55, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.5255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.00005255, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 0, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 1.9999, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 1., 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 2, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 207976931348623169, 6);
	printf("f:%30f\ns:%30s\n", a, str);fflush(stdout);free(str);

	str = ft_dtoa(a = 5255, -2);
	printf("s:%30s\n", str);fflush(stdout);free(str);
	str = ft_dtoa(a = 52.55, -2);
	printf("s:%30s\n", str);fflush(stdout);free(str);
	str = ft_dtoa(a = .5255, -2);
	printf("s:%30s\n", str);fflush(stdout);free(str);


	str = ft_dtoa(a = 5255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 52.55, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .5255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = .00005255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -5255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -52.55, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.5255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = -.00005255, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 0, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 1.9999, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 1., 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);
	str = ft_dtoa(a = 2, 0);
	printf("f:%30.f\ns:%30s\n", a, str);fflush(stdout);free(str);


	/* printf("f:%20f\ns:%s\n\n", DBL_MAX, ft_dtoa(DBL_MAX, 20)); */
	/* printf("f:%20f\ns:%s", DBL_MIN, ft_dtoa(DBL_MIN, 20)); */

//	printf("\n\nf:%f", );
	
	#include <limits.h>
//	printf("\n\n--%lu\n%.310g\n", ULONG_MAX, DBL_MAX);
/* double d = 0b0100011100011101011100001010001111010111000010100100; */
	/* str = ft_dtoa(d); */
	/* printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout); */
	/* free(str); */
	
	/* str = ft_dtoa(d = 0); */
	/* printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = -0.0001); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = 1); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = 5); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = 0x1.1F5P+0); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = 2); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */
	/* str = ft_dtoa(d = 3); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */

	/* str = ft_dtoa(d = 0.0001); */
	/* printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout); */
	/* free(str); */

	/* #include <string.h> */

	/* double a=2.132; */
	/* char arr[7]; */
	/* int i = 7; */
	/* struct s_test st; */
	/* while (--i) */
	/* 	arr[i] = 0; */

	/* printf("+++\n %ld", sizeof(a)); */
	/* memcpy(&arr,&a,sizeof(a)); */
	/* printf("+++\n ld:%ld s:%s e:%e///\n", sizeof(a), arr, a); */

	/* i = 7; */
	/* while (--i) */
	/* 	printf("%d", arr[i]); */

	/* st.d = a; */
	/* printf("\n union f:%f\ns:%s ", st.d, st.c); */
}
