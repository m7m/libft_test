/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 20:19:48 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 16:36:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putnbr_fd_test(void)
{
	int fd;

	fd = STDOUT_FILENO;
	ft_putstr(MSG_TEST1);
	ft_putstr("646 ~ ");
	ft_putnbr_fd(646, fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr_fd(1, fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr_fd(0, fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("-1 ~ ");
	ft_putnbr_fd(-1, fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("-2147483648 ~ ");
	ft_putnbr_fd(-2147483648, fd);
}
