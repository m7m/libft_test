/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:14:32 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 12:38:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memcmp_test0(char *s1, char *s2, int n)
{
	ft_putnbr(ft_memcmp(s1, s2, n));
}

void	ft_memcmp_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_memcmp_test0("asdfghjkl", "asdfghjkl", 5);
	ft_putstr(MSG_TEST1);
	ft_putstr("-14 ~ ");
	ft_memcmp_test0("asdfghjkl", "asrfghjkl", 5);
	ft_putstr(MSG_TEST1);
	ft_putstr("14 ~ ");
	ft_memcmp_test0("asrfghjkl", "asdfghjkl", 5);
}
