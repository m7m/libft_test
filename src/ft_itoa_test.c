/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 04:54:26 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 21:45:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_itoa_test0(char *val, int ival)
{
	char *citoa;

	ft_putstr(MSG_TEST1);
	citoa = ft_itoa(ival);
	ft_putstr(val);
	if (citoa == 0)
		ft_putstr("NULL");
	else
		ft_putstr(citoa);
	free(citoa);
}

void	ft_itoa_test(void)
{
	ft_itoa_test0("0 ~ ", 0);
	ft_itoa_test0("-1 ~ ", -1);
	ft_itoa_test0("1 ~ ", 1);
	ft_itoa_test0("987654321 ~ ", 987654321);
	ft_itoa_test0("-123456 ~ ", -123456);
	ft_itoa_test0("-2147483648 ~ ", -2147483648);
}
