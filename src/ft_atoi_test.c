/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 05:49:28 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:09:40 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_atoi_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("-123456 ~ ");
	ft_putnbr(ft_atoi("-123456"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-123 ~ ");
	ft_putnbr(ft_atoi("-123f456"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("-q123456"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("+q123456"));
	ft_putstr(MSG_TEST1);
	ft_putstr("1234 ~ ");
	ft_putnbr(ft_atoi("1234h56"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-2147483646 ~ ");
	ft_putnbr(ft_atoi("2147483650"));
	ft_putstr(MSG_TEST1);
	ft_putstr("-2147483648 ~ ");
	ft_putnbr(ft_atoi("-2147483648"));
	ft_putstr(MSG_TEST1);
	ft_putstr("2147483647 ~ ");
	ft_putnbr(ft_atoi("2147483647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("--2147483647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("++2147483647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("2147483647 ~ ");
	ft_putnbr(ft_atoi("+2147483647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("0\200 05 -     \t\v21474   83647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("5 ~ ");
	ft_putnbr(ft_atoi("  05 -     \t\v21474   83647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_atoi("0\200 05 -     \t\v21474   83647"));
	ft_putstr(MSG_TEST1);
	ft_putstr("2147483647 ~ ");
	ft_putnbr(ft_atoi(" +2147483647"));
}
