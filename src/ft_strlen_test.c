/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 07:27:17 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 07:34:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strlen_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("4 ~ ");
	ft_putnbr(ft_strlen("zxcv"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strlen(""));
}
