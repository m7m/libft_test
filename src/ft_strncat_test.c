/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 18:41:10 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/22 19:01:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strncat_test(void)
{
	char *dst;
	char *src;
	int i = -1;

	dst = (char *)ft_memalloc(sizeof(char) * 3);
	dst[0] = 'a';
	dst[1] = 'b';
	dst[2] = 'c';
	src = (char *)ft_memalloc(sizeof(char) * 4);
	src[0] = '1';
	src[1] = '2';
	src[2] = '3';
	src[3] = '9';
	ft_putstr(MSG_TEST1);
	ft_putstr("av: dst=abc, ap: abc[?]12 dst=abc[?]12 src=1239 ~ ");
	ft_putstr(ft_strncat(dst, src, 2));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	free(dst);
	free(src);

	dst = (char *)ft_memalloc(sizeof(char) * 1);
	dst[0] = 'a';
	dst[1] = 'b';
	dst[2] = 'c';
	src = (char *)ft_memalloc(sizeof(char) * 4);
	src[0] = '1';
	src[1] = '2';
	src[2] = '3';
	src[3] = '4';
	ft_putstr(MSG_TEST1);
	ft_putstr("av: dst=abc, ap: abc[?]12 dst=abc[?]12 src=1234 ~ ");
	ft_putstr(ft_strncat(dst, src, 2));
	ft_putstr(" dst=");
	ft_putstr(dst);
	ft_putstr(" src=");
	ft_putstr(src);
	free(dst);
	free(src);

	dst = (char *)ft_memallocset(sizeof(char) * 20, 50);
	ft_strcpy(dst, "To be ");
	dst[6] = 0;
	src = "or not to be";
	ft_putstr(MSG_TEST1);
	ft_putstr("To be or not dst=To be or not^@2222222^@ src=or not to be ~ ");
	ft_putstr(ft_strncat(dst, src, 6));
	ft_putstr(" dst=");
	while (i++ < 20)
		ft_putchar(dst[i]);
	ft_putstr(" src=");
	ft_putstr(src);
	free(dst);

	dst = (char *)ft_memallocset(sizeof(char) * 20, 50);
	ft_strcpy(dst, "To be ");
	dst[6] = 0;
	src = "or not to be";
	ft_putstr(MSG_TEST1);
	ft_putstr("To be or not dst=To be or not^@2222222^@ src=or not to be ~ ");
	ft_putstr(ft_strncat(dst, src, 6));
	ft_putstr(" dst=");
	i = -1;
	while (i++ < 20)
		ft_putchar(dst[i]);
	ft_putstr(" src=");
	ft_putstr(src);
	free(dst);
}
