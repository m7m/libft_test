/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 01:35:59 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 07:36:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_print_test(void *arg, size_t size)
{
	if (size == sizeof(char))
	{
		ft_putstr("\nchar ");
		ft_putstr(arg);
	}
	else if (size == sizeof(int))
	{
		ft_putstr("\nint ");
		ft_putnbr(*((int*)arg));
	}
	else if (size == 0)
	{
		ft_putstr("\nvoid ");
		ft_putstr("NULL");
	}
	return ;
}
