/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 02:56:20 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:14:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strnstr_test(void)
{
	char s[6];
	char *ret;

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("bce ~ ");
	ret = ft_strnstr(s, "bc", 4);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'b';
	s[3] = 'c';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("bbce ~ ");
	ret = ft_strnstr(s, "b", 4);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	s[0] = 0;
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strnstr(s, "bc", 3);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strnstr(s, "bc", 2);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("abcde ~ ");
	ret = ft_strnstr(s, "", 5);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	ret = ft_strnstr(s, "be", 5);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ret = ft_strnstr("\0", "\0", 0);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ret = ft_strnstr("\0", "\0", 1);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);

	ft_putstr(MSG_TEST1);
	ft_putstr("ozaraboze123 ~ ");
	ret = ft_strnstr("ozarabozaraboze123", "ozaraboze", 15);
	if (ret == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ret);
}
