/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 07:57:32 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 22:12:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memset_test(void)
{
	char *str;

	str = (char *)ft_memalloc(sizeof(char) * 4);
	ft_putstr(MSG_TEST1);
	ft_putstr("^[^[^[ ~ ");
	ft_memset(str, 27, 3);
	ft_putstr(str);
	ft_putstr(MSG_TEST1);
	ft_putstr("KKKK[.*] ~ ");
	ft_memset(str, 75, 4);
	ft_putstr(str);
	free(str);
}
