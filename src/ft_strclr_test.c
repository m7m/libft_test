/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 19:09:13 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 19:30:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strclr_test(void)
{
	char str[5] = "asdfg";
	int i = 4;

	
	ft_putstr(MSG_TEST1);
	ft_putstr("^@^@^@^@ ~ ");
	ft_strclr(str);
	while (i--)
		ft_putchar(str[i]);
}
