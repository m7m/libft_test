/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 07:04:23 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 12:22:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putnbr_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("-453436 ~ ");
	ft_putnbr(-453436);
	ft_putstr(MSG_TEST1);
	ft_putstr("65343 ~ ");
	ft_putnbr(65343);
	ft_putstr(MSG_TEST1);
	ft_putstr("27363 ~ ");
	ft_putnbr(065343);
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(0);
	ft_putstr(MSG_TEST1);
	ft_putstr("-2147483648 ~ ");
	ft_putnbr(-2147483648);
	ft_putstr(MSG_TEST1);
	ft_putstr("-1 ~ ");
	ft_putnbr(-01);
	ft_putstr(MSG_TEST1);
	ft_putstr("-1 ~ ");
	ft_putnbr(-1);
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(1);
}
