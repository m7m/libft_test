/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 16:58:07 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/30 17:08:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include <ctype.h>

void	ft_isprint_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_isprint(48));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(isprint(48));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	char *a = "\200";
	ft_putnbr(ft_isprint(a[0]));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_isprint(a[0]));
}
