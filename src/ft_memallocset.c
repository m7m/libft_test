/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memallocset.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 21:32:26 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/26 09:50:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Alloue de la memoire, initialise a 'c' et fini par 0
** Retourne la zone memoire fraichement alloue
*/

#include "libft.h"
#include "libft_test.h"

void	*ft_memallocset(size_t size, int c)
{
	char *s;

	s = malloc(size);
	if (!s)
		return (NULL);
	s[size] = 0;
	while (size--)
		((unsigned char*)s)[size] = (unsigned char)c;
	return (s);
}
