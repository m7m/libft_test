/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:02:41 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 22:21:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memchr_test(void)
{
	char *str = "qwe";
	char *ret;

	ft_putstr(MSG_TEST1);
	ft_putstr("we ~ ");
	if ((ret = (char *)ft_memchr(str, 119, 3)))
		ft_putstr(ret);
	else
		ft_putstr("NULL");
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	if ((ret = (char *)ft_memchr(str, 10, 3)))
		ft_putstr(ret);
	else
		ft_putstr("NULL");
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	str = "";
	if ((ret = (char *)ft_memchr(str, 49, 3)))
		ft_putstr(ret);
	else
		ft_putstr("NULL");
}
