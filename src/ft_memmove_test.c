/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:59:59 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/18 06:20:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memmove_test0(char *dst, char *src)
{
  char *ret;
	ft_putstr("dst=");
	ft_putstr(dst);
	ft_putstr(",src=");
	ft_putstr(src);
	ft_putstr(",mv=");
	if ((ret = memmove(dst, src, 2)))
	  ft_putstr(ret);
	else
	  ft_putstr("NULL");
}

void	ft_memmove_test(void)
{
	char src[4] = "qwe";
	char dst[4] = "123";

	ft_putstr(MSG_TEST1);
	ft_putstr("dst=123,src=qwe,mv=qw3 ~ ");
	ft_memmove_test0(dst, src);
	ft_putstr(MSG_TEST1);
	ft_putstr("dst=123,src=789,mv=783 ~ ");
	dst[0] = 49;
	dst[1] = 50;
	dst[2] = 51;
	dst[3] = 0;
	ft_memmove_test0(dst, "789");
}

void ft_TestMemMove(void)
{
	char chn1[] = "abcdefghijklmno";
	char chn2[] = "abcdefghijklmno";
	char chn3[] = "abcdefghijklmno";
	char chn4[] = "abcdefghijklmno";
	char chn5[] = "abcdefghijklmno";
	char chn6[] = "abcdefghijklmno";
 
	memmove(chn1, chn1+5, 10); /*Décalage de 5 vers la gauche*/
	ft_memmove(chn3, chn3+5, 10); /*Décalage de 5 vers la gauche*/
	ft_putstr(MSG_TEST1);
	ft_putstr(chn1);
	ft_putstr(" ~ ");
	ft_putstr(chn3);

	memmove(chn2+5, chn2, 10); /*Décalage de 5 vers la droite*/
	ft_memmove(chn4+5, chn4, 10); /*Décalage de 5 vers la droite*/
	ft_putstr(MSG_TEST1);
	ft_putstr(chn2);
	ft_putstr(" ~ ");
	ft_putstr(chn4);
	memmove(chn5, chn5+6, 8); /*Décalage de 5 vers la droite*/
	ft_memmove(chn6, chn6+6, 8); /*Décalage de 5 vers la gauche*/
	ft_putstr(MSG_TEST1);
	ft_putstr(chn5);
	ft_putstr(" ~ ");
	ft_putstr(chn6);
}
