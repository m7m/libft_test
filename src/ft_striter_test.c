/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 20:36:16 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 20:44:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_striter_test0(char *c)
{
	*c += 32;
}

void	ft_striter_test(void)
{
	char str[] = "ASDFGZ"; 
	ft_putstr(MSG_TEST1);
	ft_putstr("asdfgz ~ ");
	ft_striter(str, &ft_striter_test0);
	ft_putstr(str);
}
