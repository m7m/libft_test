/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 00:06:36 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/12 13:26:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strtrim_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr(ft_strtrim(""));
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr(ft_strtrim("      "));
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_putstr(ft_strtrim("   \t\n \t "));
	ft_putstr(MSG_TEST1);
	ft_putstr("sdgjp sf sdg ~ ");
	ft_putstr(ft_strtrim("  sdgjp sf sdg  "));
	ft_putstr(MSG_TEST1);
	ft_putstr("sdf   dg;\tbdg ~ ");
	ft_putstr(ft_strtrim("sdf   dg;\tbdg"));
	ft_putstr(MSG_TEST1);
	ft_putstr("ddb d ~ ");
	ft_putstr(ft_strtrim("\t\nddb d"));
	ft_putstr(MSG_TEST1);
	ft_putstr("ddb d ~ ");
	ft_putstr(ft_strtrim("ddb d\n\n\n"));
	ft_putstr(MSG_TEST1);
	ft_putstr("ddb dv ~ ");
	ft_putstr(ft_strtrim("\tddb dv   "));
	ft_putstr(MSG_TEST1);
	ft_putstr("sdgjp sf sdg ~ ");
	ft_putstr(ft_strtrim(" sdgjp sf sdg "));

}
