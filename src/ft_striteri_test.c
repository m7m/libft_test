/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 20:44:56 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 23:36:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_striteri_test0(unsigned int i, char *c)
{
	*c = i + 48;
}

void	ft_striteri_test(void)
{
	char str[] = "basdfgz"; 
	ft_putstr(MSG_TEST1);
	ft_putstr("0123456 ~ ");
	ft_striteri(str, &ft_striteri_test0);
	ft_putstr(str);
}
