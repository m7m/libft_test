/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 08:10:13 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/28 10:12:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_toupper_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("P ~ ");
	ft_putchar(ft_toupper('P'));
	ft_putstr(MSG_TEST1);
	ft_putstr("U ~ ");
	ft_putchar(ft_toupper('U'));
	ft_putstr(MSG_TEST1);
	ft_putstr("5 ~ ");
	ft_putchar(ft_toupper('5'));
	ft_putstr(MSG_TEST1);
	ft_putstr("# ~ ");
	ft_putchar(ft_toupper('#'));
	ft_putstr(MSG_TEST1);
	ft_putstr("127 ~ ");
	ft_putnbr(ft_toupper(0177));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_toupper(0));
	ft_putstr(MSG_TEST1);
	ft_putstr("255 ~ ");
	ft_putnbr(ft_toupper(0xff));
}
