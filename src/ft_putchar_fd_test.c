/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 19:55:53 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 16:36:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putchar_fd_test(void)
{
	int fd;
	char c;

	c = 0;
	//fd = open(STDOUT_FILENO, O_CREAT | O_WRONLY, 0644);
	fd = STDOUT_FILENO;
	ft_putstr(MSG_TEST1);
	ft_putstr("a ~ ");
	ft_putchar_fd('a', fd);
	ft_putstr(MSG_TEST1);
	ft_putstr("\\0 ~ ");
	ft_putchar_fd(c, fd);
}
