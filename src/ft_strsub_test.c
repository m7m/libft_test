/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 23:52:08 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/03 00:03:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strsub_test(void)
{
	char str[] = "basdfgz";
	char *strsub;

	ft_putstr(MSG_TEST1);
	ft_putstr("sdf basdfgz ~ ");
	strsub = ft_strsub(str, 2, 3);
	ft_putstr(strsub);
	ft_putchar(' ');
	ft_putstr(str);
	free(strsub);
	ft_putstr(MSG_TEST1);
	ft_putstr("basdfgz basdfgz ~ ");
	strsub = ft_strsub(str, 0, 7);
	ft_putstr(strsub);
	ft_putchar(' ');
	ft_putstr(str);
	free(strsub);
	ft_putstr(MSG_TEST1);
	ft_putstr("[.*] basdfgz ~ ");
	strsub = ft_strsub(str, 8, 20);
	ft_putstr(strsub);
	ft_putchar(' ');
	ft_putstr(str);
	free(strsub);
}
