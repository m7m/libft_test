/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 00:03:16 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:02:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"
#include <stdio.h>
void	ft_strchr_test(void)
{
	char s[6];

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("cde ~ ");
	ft_putstr(ft_strchr(s, 'c'));

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'c';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("cce ~ ");
	ft_putstr(ft_strchr(s, 'c'));

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("NULL ~ ");
	if (ft_strchr(s, 'g') == NULL)
		ft_putstr("NULL");
	else
		ft_putstr("non NULL");

	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	ft_putstr(MSG_TEST1);
	ft_putstr("ok ~ ");
	if (strchr("Je suis un poisson.", 0) == ft_strchr("Je suis un poisson.", 0))
		ft_putstr("ok");
}
