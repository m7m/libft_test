/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 19:42:17 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:42:04 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strnequ_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strnequ("asdfg", "asdfg", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strnequ("asdfg", "qsdfg", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strnequ("asdfg", "asdog", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strnequ("qsdfg", "asdfg", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strnequ("asdog", "asdfg", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strnequ("asdog", "", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strnequ("", "asdog", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strnequ("", "", 3));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strnequ("asdog", "", 0));
}
