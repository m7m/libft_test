/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 09:21:16 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/21 23:40:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memcpy_test(void)
{
	char src[4] = "456";
	char dst[4] = "321";
	char *orig;

	ft_putstr(MSG_TEST1);
	ft_putstr("orig=451 src=456 dst=451 ~ orig=");
	ft_putstr(orig = ft_memcpy(dst, src, 2));
	ft_putstr(" src=");
	ft_putstr(src);
	ft_putstr(" dst=");
	ft_putstr(dst);
}

void ft_TestMemCpy(void)
{
	char chn1[] = "abcdefghijklmno";
	char chn2[] = "abcdefghijklmno";
	char chn3[] = "abcdefghijklmno";
	char chn4[] = "abcdefghijklmno";
	char chn5[] = "abcdefghijklmno";
	char chn6[] = "abcdefghijklmno";
 
	ft_putstr(MSG_TEST1);
	ft_putstr((char *)memcpy(chn1, chn1+5, 10)); /*Décalage de 5 vers la gauche*/
	ft_putchar(' ');
	ft_putstr(chn1);
	ft_putstr(" ~ ");
	ft_putstr((char *)ft_memcpy(chn3, chn3+5, 10)); /*Décalage de 5 vers la gauche*/
	ft_putchar(' ');
	ft_putstr(chn3);

	ft_putstr(MSG_TEST1);
	ft_putstr((char *)memcpy(chn2+5, chn2, 10)); /*Décalage de 5 vers la droite*/
	ft_putchar(' ');
	ft_putstr(chn2);
	ft_putstr(" ~ ");
	ft_putstr((char *)ft_memcpy(chn4+5, chn4, 10)); /*Décalage de 5 vers la droite*/
	ft_putchar(' ');
	ft_putstr(chn4);
	
	ft_putstr(MSG_TEST1);
	ft_putstr((char *)memcpy(chn5, chn5+6, 8)); /*Décalage de 5 vers la droite*/
	ft_putchar(' ');
	ft_putstr(chn5);
	ft_putstr(" ~ ");
	ft_putstr((char *)ft_memcpy(chn6, chn6+6, 8)); /*Décalage de 5 vers la gauche*/
	ft_putchar(' ');
	ft_putstr(chn6);
}
// dst = abcdefghijklmno
// src = fghijklmno
// fghij klmno klmno

// dst = fghijklmno
// src = abcdefghij klmno
// abcde abcde fghde

// dst = 0123456789
// src = abcde01234 56789
// abcde ab234 56789
