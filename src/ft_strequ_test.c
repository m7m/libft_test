/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 20:33:18 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 20:35:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strequ_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strequ("asdfg", "asdfg"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("asdfg", "qsdfg"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("asdfg", "asdog"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("qsdfg", "asdfg"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("asd", "asdfg"));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("asdog", ""));
	ft_putstr(MSG_TEST1);
	ft_putstr("0 ~ ");
	ft_putnbr(ft_strequ("", "asdog"));
	ft_putstr(MSG_TEST1);
	ft_putstr("1 ~ ");
	ft_putnbr(ft_strequ("", ""));
}
