/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 23:12:03 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 23:26:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strjoin_test(void)
{
	char *str;

	ft_putstr(MSG_TEST1);
	ft_putstr("abcdEFGH ~ ");
	str = ft_strjoin("abcd", "EFGH");
	ft_putstr(str);
	free(str);
}
