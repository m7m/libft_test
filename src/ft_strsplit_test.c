/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 09:40:50 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:46:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strsplit_test2(char *str, char c)
{
	char **ss;

	ss = ft_strsplit(str, c);
	if (*ss)
	{
		ft_putstr("1");
		ft_putstrf(*ss++, '2');
		if (*ss)
		{
			ft_putstr("3");
			ft_putstrf(*ss++, '4');
			if (*ss)
			{
				ft_putstr("5");
				ft_putstrf(*ss++, '6');
				if (*ss)
				{
					ft_putstr("7");
					ft_putstrf(*ss++, '8');
				}
			}
		}
	}
}

void	ft_strsplit_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut23les45etudiants6 ~ ");
	ft_strsplit_test2("*salut*les***etudiants*", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut23les45etudiants6 ~ ");
	ft_strsplit_test2("*salut*les***etudiants", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut23les45etudiants6 ~ ");
	ft_strsplit_test2("salut*les***etudiants*", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut-les23etudiants4 ~ ");
	ft_strsplit_test2("*salut-les*etudiants", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut23les45etudiants6 ~ ");
	ft_strsplit_test2("*salut*les***etudiants", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1*salut*les***etudiants*2 ~ ");
	ft_strsplit_test2("*salut*les***etudiants*", ' ');
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_strsplit_test2("", ' ');
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_strsplit_test2("   ", ' ');
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	ft_strsplit_test2("*********", '*');
	ft_putstr(MSG_TEST1);
	ft_putstr("1salut2 ~ ");
	ft_strsplit_test2("****salut", '*');

}
