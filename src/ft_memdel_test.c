/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 01:27:08 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 04:52:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memdel_test(void)
{
	char **var_ft_memdel01;
	char *var_ft_memdel02;

	var_ft_memdel02 = (char *)malloc(sizeof(char) * 4);
	var_ft_memdel01 = &var_ft_memdel02;
	ft_putstr(MSG_TEST1);
	ft_putstr("ab");
	ft_putstr(" ~ ");
	var_ft_memdel02[0] = 'a';
	var_ft_memdel02[1] = 'b';
	var_ft_memdel02[2] = 0;
	if (var_ft_memdel02 != NULL)
		ft_putstr(var_ft_memdel02);
	ft_memdel((void *)var_ft_memdel01);
	if (var_ft_memdel02 == NULL)
		ft_putstr(", NULL = NULL");
	else
		ft_putstr(", NULL = non NULL");
}
