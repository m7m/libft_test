/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:42:00 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/27 23:52:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strncpy_test(void)
{
	char *dst;

	dst = "qwertyu";
	dst = (char *)ft_memalloc(sizeof(char) * 10);
	dst = (char *)ft_memset(dst, 27, sizeof(char) * 10);
	if (dst == NULL)
		ft_putstr("NULL");
	ft_putstr(MSG_TEST1);
	ft_putstr("123456789123456 segment fault ~ ");
	dst = ft_strncpy(dst, "123456789123456789123456789", 15);
	ft_putstr(dst);
	ft_putstr(MSG_TEST1);
	ft_putstr("-----6789123456 ~ ");
	dst = ft_strncpy(dst, "-----------------------", 5);
	ft_putstr(dst);
	ft_putstr(MSG_TEST1);
	ft_putstr("oooo ~ ");
	dst = ft_strncpy(dst, "oooo", 10);
	ft_putstr(dst);
	free(dst);
}
