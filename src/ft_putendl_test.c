/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 18:41:15 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 19:04:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_putendl_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("asd\\n ~ ");
	ft_putendl("asd");
	ft_putstr(MSG_TEST1);
	ft_putstr("\\n ~ ");
	ft_putendl("");
}
