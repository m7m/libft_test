/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:44:43 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 18:49:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_strdup_test2(char c, char t)
{
	char *dustr;
	char *test;
	char *str;

	str = (char *)ft_memallocset(sizeof(char) * 5, c);
	test = (char *)ft_memallocset(sizeof(char) * 5, t);
	ft_putstr("s=");
	ft_putstrf(str, ' ');
	dustr = ft_strdup(str);
	ft_memmove(dustr, test, ft_strlen(dustr));
	ft_putstr("t=");
	ft_putstrf(test, ' ');
	ft_putstr("dus=");
	ft_putstrf(dustr, ' ');
	ft_putstr("s=");
	str[2] = 48;
	ft_putstr(str);
	free(str);
	free(test);
	free(dustr);
}

void	ft_strdup_test(void)
{
	ft_putstr(MSG_TEST1);
	ft_putstr("s=11111 t=22222 dus=22222 s=11011 ~ ");
	ft_strdup_test2(49, 50);
	ft_putstr(MSG_TEST1);
	ft_putstr("s= t=22222 dus= s= ~ ");
	ft_strdup_test2(0, 50);
	ft_putstr(MSG_TEST1);
	ft_putstr("s= t= dus= s= ~ ");
	ft_strdup_test2(0, 0);
	ft_putstr(MSG_TEST1);
	ft_putstr("s=11111 t= dus= s=11011 ~ ");
	ft_strdup_test2(49, 0);
}
