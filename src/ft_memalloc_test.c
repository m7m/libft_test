/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 03:53:20 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/22 23:25:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_test.h"

void	ft_memalloc_test(void)
{
	int		*var1;
	char	*var2;

	ft_putstr(MSG_TEST1);
	ft_putstr("10");
	ft_putstr(" ~ ");
	var1 = (int *)ft_memalloc(4);
	*var1 = 10;
	ft_putnbr(*var1);
	free(var1);
	ft_putstr(MSG_TEST1);
	ft_putstr("asdf");
	ft_putstr(" ~ ");
	var2 = (char *)ft_memalloc(sizeof(char) * 7);
	var2[0] = 'a';
	var2[1] = 's';
	var2[2] = 'd';
	var2[3] = 'f';
	var2[5] = 'G';
	ft_putstr(var2);
	free(var2);

	var2[0] = 0;
	ft_putstr(MSG_TEST1);
	ft_putstr(" ~ ");
	var2 = (char *)ft_memalloc(0);
	if (!var2)
		ft_putstr("NULL");
	else
		ft_putstr(var2);
	free(var2);
}
