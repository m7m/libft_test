#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/18 06:53:03 by mmichel           #+#    #+#              #
#    Updated: 2016/05/24 15:01:02 by mmichel          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

PREFIX	:= .
INC := $(PREFIX)/inc
LIBS := $(PREFIX)/lib
SRC := $(PREFIX)/src
LIBFT := $(PREFIX)/libft
LIBFTA := $(LIBFT)/libft.a

SRCLIBC = $(shell find $(SRC) -type f | grep ".c$$" \
	| grep -vE "(/\.|main\.c|/\#)" )

SRCLIBFT = $(shell find $(LIBFT) -type f | grep ".c$$" \
	| grep -vE "(/\.|main\.c|/\#)" )

OBJ = $(SRCLIBC:.c=.o)

BINC = $(SRC)/$(COMPILE).c

CC := gcc
LDLIBS := ${LDLIBS} -L$(LIBS)
LDFLAGS := ${LDFLAGS} -lft_test
CFLAGS := -Wall -Werror -Wextra -I$(INC) -I$(LIBFT)
COMPILE = test-bin
LIBA = $(LIBS)/libft_test.a
NAME = $(COMPILE)

all: $(COMPILE)

$(COMPILE): $(LIBFTA) $(LIBA)
	@echo Compile $(COMPILE)
	$(CC) -o $@ $(SRC)/main.c $(CFLAGS) $(LDLIBS) $(LDFLAGS) $^

$(LIBFTA):
	git submodule update --init
	$(MAKE) -C $(LIBFT) $(MFLAGS)

$(LIBA): $(OBJ) $(SRCLIBFT:.c=.o)
	$(AR) crs $@ $^
	ranlib $@

clean:
	$(RM) $(OBJ)
	$(RM) $(LIBA)
	$(MAKE) -C $(LIBFT) clean

fclean: clean
	$(RM) $(COMPILE)
	$(MAKE) -C $(LIBFT) fclean

test: fclean $(COMPILE)
	@echo "---------------------------------------\n"
	./$(COMPILE) $(OPT_TEST) | cat -e
	@echo "\n---------------------------------------"
